Iron.io Extension for Yii 2
================================

**Attention**: Please do not use in production environments. It‘s WIP.

This extension integrates [iron.io](http://www.iron.io/) services with the Yii2 Framework.

This extension provides building and uploading of iron worker based console applications. You can run them remotely in iron.io or locally.

This extension is heavily expired by the [yiiron](https://github.com/br0sk/yiiron) extension by [br0sk](https://github.com/br0sk) for [yii v1](https://github.com/yiisoft/yii).


Requirements
------------

This yii2 extension depends on the official iron.io php packages:
    
* [iron-io/iron_worker: 2.0.*](https://github.com/iron-io/iron_worker_php)
* [iron-io/iron_mq: 2.0.*](https://github.com/iron-io/iron_mq_php) (Does only support API v1, not new AP v3)

It uses special namespace patched version for iron cache:

* [iron-io/iron_cache: 1.0.0-rc.1](https://github.com/spacedealer/iron_cache_php)

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist spacedealer/yii2-iron-io "*"
```

or add

```
"spacedealer/yii2-iron-io": "*"
```

to the require section of your `composer.json` file.


Usage
-----

TBD

Once the extension is installed, simply modify your application configuration as follows:

```php
//tbd.
```
